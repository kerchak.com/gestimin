import java.util.Scanner;

public class Gestimin {
// N determina el tamaño del array
static int N = 100;
public static void main(String[] args) {
int opcion;
int primeraLibre;
int i;
int stockIntroducido;
double precioDeCompraIntroducido;
double precioDeVentaIntroducido;
String codigo;
String codigoIntroducido = "";
String descripcionIntroducida;
String precioDeCompraIntroducidoString;
String precioDeVentaIntroducidoString;
String stockIntroducidoString;
boolean existeCodigo;
//Crea el array de artículos
Articulo[] articulo = new Articulo[N];
// Crea todos los artículos que van en cada una de
// las celdas del array
for(i = 0; i < N; i++) {
articulo[i] = new Articulo();
}
// Menu
do {
System.out.println("\n\nGESTIMIN");
System.out.println("===================");
System.out.println("1. Listado");
System.out.println("2. Alta");
System.out.println("3. Baja");
System.out.println("4. Modificación");
System.out.println("5. Entrada de mercancía");
System.out.println("6. Salida de mercancía");
System.out.println("7. Salir");
System.out.print("Introduzca una opción: ");
Scanner inpu = new Scanner(System.in);
opcion = inpu.nextInt();
switch (opcion) {
/////////////////////////////////////////////////////////////////////////////
// LISTADO //////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
case 1:
System.out.println("\nLISTADO");
System.out.println("=======");
for(i = 0; i < N; i++) {
if (!articulo[i].getCodigo().equals("LIBRE")) {
System.out.println(articulo[i]);
}
}
break;
/////////////////////////////////////////////////////////////////////////////
// ALTA /////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
case 2:
System.out.println("\nNUEVO ARTÍCULO");
System.out.println("==============");
// Busca la primera posición libre del array
primeraLibre = 0;
codigo = articulo[primeraLibre].getCodigo();
while ((primeraLibre < N) && (!(codigo.equals("LIBRE")))) {
primeraLibre++;
if (primeraLibre < N) {
codigo = articulo[primeraLibre].getCodigo();
}
}
if (primeraLibre == N) {
System.out.println("Lo siento, a base de datos está llena.");
} else {
// Introducción de datos
System.out.println("Por favor, introduzca los datos del artículo.");
System.out.print("Código: ");
// Comprueba que el código introducido no se repita
existeCodigo = true;
while (existeCodigo) {
existeCodigo = false;
Scanner input2 = new Scanner(System.in);
codigoIntroducido = input2.nextLine();

for (i = 0; i < N; i++) {
if (codigoIntroducido.equals(articulo[i].getCodigo())) {
existeCodigo = true;
}
}
if (existeCodigo) {
System.out.println("Ese código ya existe en la base de datos.");
System.out.print("Introduzca otro código: ");
}
} // while (existeCodigo)
articulo[primeraLibre].setCodigo(codigoIntroducido);
System.out.print("Descripcion: ");

Scanner input3 = new Scanner(System.in);
descripcionIntroducida = input3.nextLine();

articulo[primeraLibre].setDescripcion(descripcionIntroducida);
System.out.print("Precio de compra: ");

Scanner input4 = new Scanner(System.in);
precioDeCompraIntroducido = input4.nextInt();

articulo[primeraLibre].setPrecioDeCompra(precioDeCompraIntroducido);
System.out.print("Precio de venta: ");

Scanner input5 = new Scanner(System.in);
precioDeVentaIntroducido = input5.nextInt();

articulo[primeraLibre].setPrecioDeVenta(precioDeVentaIntroducido);
System.out.print("Stock: ");

Scanner input2555 = new Scanner(System.in);
stockIntroducido = input2555.nextInt();
articulo[primeraLibre].setStock(stockIntroducido);
}
break;
/////////////////////////////////////////////////////////////////////////////
// BAJA /////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
case 3:
System.out.println("\nBAJA");
System.out.println("====");
System.out.println("Por favor, introduzca el código del artículo que desea dar de baja");

Scanner input6 = new Scanner(System.in);
codigoIntroducido = input6.nextLine();

i = -1;
codigo = "";
do {
i++;
if (i < N) {
codigo = articulo[i].getCodigo();
}
} while (!(codigo.equals(codigoIntroducido)) && (i < N));
if (i == N) {
System.out.println("Lo siento, el código introducido no existe.");
} else {
articulo[i].setCodigo("LIBRE");
System.out.println("articulo borrado.");
}
break;
/////////////////////////////////////////////////////////////////////////////
// MODIFICACIÓN /////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
case 4:
System.out.println("\nMODIFICACIÓN");
System.out.println("============");
System.out.print("Por favor, introduzca el código del artículo cuyos datos desea cambiar: ");
Scanner input8 = new Scanner(System.in);
codigoIntroducido = input8.nextLine();
i = -1;
do {
i++;
} while (!((articulo[i].getCodigo()).equals(codigoIntroducido)));
System.out.println("Introduzca los nuevos datos del artículo o INTRO para dejarlos igual.");
System.out.println("Código: " + articulo[i].getCodigo());
System.out.print("Nuevo código: ");

Scanner input9 = new Scanner(System.in);
codigoIntroducido = input9.nextLine();

if (!codigoIntroducido.equals("")) {
articulo[i].setCodigo(codigoIntroducido);
}
System.out.println("Descripción: " + articulo[i].getDescripcion());
System.out.print("Nueva descripción: ");

Scanner input209 = new Scanner(System.in);
descripcionIntroducida = input209.nextLine();
if (!descripcionIntroducida.equals("")) {
articulo[i].setDescripcion(descripcionIntroducida);
}
System.out.println("Precio de compra: " + articulo[i].getPrecioDeCompra());
System.out.print("Nuevo precio de compra: ");

Scanner input10 = new Scanner(System.in);
precioDeCompraIntroducidoString = input10.nextLine();

if (!precioDeCompraIntroducidoString.equals("")) {
articulo[i].setPrecioDeCompra(Double.parseDouble(precioDeCompraIntroducidoString));
}
System.out.println("Precio de venta: " + articulo[i].getPrecioDeVenta());
System.out.print("Nuevo precio de venta: ");

Scanner input11 = new Scanner(System.in);
precioDeVentaIntroducidoString = input11.nextLine();

if (!precioDeVentaIntroducidoString.equals("")) {
articulo[i].setPrecioDeVenta(Double.parseDouble(precioDeVentaIntroducidoString));
}
System.out.println("Stock: " + articulo[i].getStock());
System.out.print("Nuevo stock: ");

Scanner input12 = new Scanner(System.in);
stockIntroducidoString = input12.nextLine();

if (!stockIntroducidoString.equals("")) {
articulo[i].setStock(Integer.parseInt(stockIntroducidoString));
}
break;
/////////////////////////////////////////////////////////////////////////////
// ENTRADA DE MERCANCÍA /////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
case 5:
System.out.println("\nENTRADA DE MERCANCÍA");
System.out.println("====================");
System.out.print("Por favor, introduzca el código del artículo: ");

Scanner input13 = new Scanner(System.in);
codigoIntroducido = input13.nextLine();

i = -1;
codigo = "";
do {
i++;
if (i < N) {
codigo = articulo[i].getCodigo();
}
} while (!(codigo.equals(codigoIntroducido)) && (i < N));
if (i == N) {
System.out.println("Lo siento, el código introducido no existe.");
} else {
System.out.println("Entrada de mercancía del siguiente artículo: ");
System.out.println(articulo[i]);
System.out.print("Introduzca el número de unidades que entran al almacén: ");
Scanner input14 = new Scanner(System.in);
stockIntroducidoString = input14.nextLine();

articulo[i].setStock(Integer.parseInt(stockIntroducidoString) + articulo[i].getStock());
System.out.println("La mercancía ha entrado en el almacén.");
}
break;
/////////////////////////////////////////////////////////////////////////////
// SALIDA DE MERCANCÍA //////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
case 6:
System.out.println("\nSALIDA DE MERCANCÍA");
System.out.println("===================");
System.out.print("Por favor, introduzca el código del artículo: ");

Scanner input15 = new Scanner(System.in);
codigoIntroducido = input15.nextLine();
i = -1;
codigo = "";
do {
i++;
if (i < N) {
codigo = articulo[i].getCodigo();
}
} while (!(codigo.equals(codigoIntroducido)) && (i < N));
if (i == N) {
System.out.println("Lo siento, el código introducido no existe.");
} else {
System.out.println("Salida de mercancía del siguiente artículo: ");
System.out.println(articulo[i]);
System.out.print("Introduzca el número de unidades que desea sacar del almacén: ");
Scanner input16 = new Scanner(System.in);
stockIntroducido = input16.nextInt();

if (articulo[i].getStock() - stockIntroducido > 0) {
articulo[i].setStock(articulo[i].getStock() - stockIntroducido);
System.out.println("La mercancía ha salido del almacén.");
} else {
System.out.println("Lo siento, no se pueden sacar tantas unidades.");
}
}
break; 
} }
while (opcion != 7); 
 
}
}